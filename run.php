<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 07/02/2018
 * Time: 23:03
 */

require __DIR__ . '/vendor/autoload.php';
// Define our indexes as constants for readability
const INDEX_IP   = 1,
INDEX_DATE       = 4,
INDEX_TIME       = 5,
INDEX_METHOD     = 7,
INDEX_SCRIPT     = 8,
INDEX_PROTOCOL   = 9,
INDEX_STATUS     = 10,
INDEX_REFFERER   = 12,
INDEX_USER_AGENT = 13;


// Add new things to report in here
$dataSetMap =
    [
        INDEX_REFFERER => \HttpReport\Common\ReferralDataSet::class,
        INDEX_SCRIPT   => \HttpReport\Common\ScriptDataSet::class,
        INDEX_USER_AGENT => \HttpReport\Common\UserAgentDataSet::class,
        INDEX_STATUS => \HttpReport\Common\UserAgentDataSet::class,
    ];

// Create a new parser object
$parser = new \HttpReport\Core\Parser(file('./access_log'));

// Run the console report type.
$console = new \HttpReport\Core\ConsoleReporter($parser);

