<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 08/02/2018
 * Time: 00:11
 */

namespace HttpReport\Common;


use HttpReport\Core\DataSet;

class UserAgentDataSet extends DataSet
{

    function getName(): String
    {
        return 'User Agent';
    }

    function getMutatedValue($value)
    {
        return $value;
    }
}
