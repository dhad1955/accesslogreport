<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 08/02/2018
 * Time: 00:11
 */

namespace HttpReport\Common;


use HttpReport\Core\DataSet;

class ReferralDataSet extends DataSet
{
    function getName(): String
    {
        return 'Referrer';
    }

    function getMutatedValue($value)
    {
        if($value == '-') {
            return 'Direct';
        }

        return preg_replace('/\?.*/', '', $value);
    }
}
