<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 08/02/2018
 * Time: 00:30
 */

namespace HttpReport\Common;


use HttpReport\Core\DataSet;

class ResponseCodeDataSet extends DataSet
{

    function getName(): String
    {
        return 'Response code';
    }

    function getMutatedValue($value)
    {
        return $value;
    }

}
