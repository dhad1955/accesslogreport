<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 07/02/2018
 * Time: 23:17
 */

namespace HttpReport\Common;
use HttpReport\Core\DataSet;

class ScriptDataSet extends DataSet
{

    function getName() : String
    {
        return "Script";
    }

    function getMutatedValue($value)
    {
        // Remove the query string
        return preg_replace('/\?.*/', '', $value);
    }
}
