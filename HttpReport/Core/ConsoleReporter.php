<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 08/02/2018
 * Time: 00:33
 */

namespace HttpReport\Core;


class ConsoleReporter
{

    public function __construct(Parser $parser)
    {
        $this->comment('Access log extractor 1.00');
        $this->comment('Please wait parsing..');
        $dataSets = $parser->getParsedDataSets();
        $this->comment("Parsed {$parser->count()} records");

        // Report our HTTP status
        if (isset($dataSets[INDEX_STATUS])) {
            // number of errors
            $errorCount   = 0;
            // number of succesful requests
            $successCount = 0;
            // get a status report for http response codes
            $statusReport = $dataSets[INDEX_STATUS]->getExtendedData('DESC');
            // iterate through each response code
            foreach ($statusReport AS $statusCode => $data) {
                $codeAsString = (string)$statusCode;
                // the prefix is the first segment of the code ie (4.x.x, 2.x.x)
                $prefix       = $codeAsString[0];
                // error codes
                if ($prefix == 4 || $prefix == 5) {
                    $errorCount += $data['count'];
                } else {
                    $successCount += $data['count'];
                }
            }
            $this->comment('Errors: ' . $errorCount);
            $this->comment('Success count: ' . $successCount);
        }
        $this->report($dataSets[INDEX_SCRIPT]);
        $this->report($dataSets[INDEX_REFFERER]);
        $this->report($dataSets[INDEX_USER_AGENT]);
    }

    private function report(DataSet $dataSet) : void
    {
        $count = 10;
        $this->comment("Generating report for {$dataSet->getName()} displaying top {$count}");

        foreach($dataSet
            ->getExtendedData('DESC', $count)
            as $key => $value) {

            $this->comment("{$key} | {$value['count']} | {$value['percentage']} ");
        }
        $this->comment();
        $this->comment();
    }
    private function comment($comment ='')
    {
        echo '--- ' . $comment . PHP_EOL;
    }

}
