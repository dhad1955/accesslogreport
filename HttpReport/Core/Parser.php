<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 07/02/2018
 * Time: 23:31
 */

namespace HttpReport\Core;


class Parser
{
    private $lines;
    private $dataSets = [];
    private $count = 0;

    public function __construct(array $lines)
    {
        $this->lines = $lines;
    }

    public function getParsedDataSets()
    {
        global $dataSetMap;

        foreach ($this->lines as $line) {
            $matches = [];
            // Regex to parse the extended log format
            $regex = '/^(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) "([^"]*)" "([^"]*)"$/';
            preg_match($regex, $line, $matches);

            $didTrack = false;
            foreach (array_keys($dataSetMap) as $key) {
                $didTrack |= $this->track($key, $matches);
            }

            if ($didTrack) {
                $this->count++;
            }
        }

        return $this->dataSets;
    }

    // Track a new item
    private function track($index, array $matches)
    {
        global $dataSetMap;

        if (count($matches) < $index) {
            // not a valid line
            return false;
        }

        if ( ! isset($this->dataSets[$index])) {
            if ( ! isset($dataSetMap[$index])) {
                return false; // Not in our config so we don't care about it.
            }
            $this->dataSets[$index] = new $dataSetMap[$index]();
        }

        $this->dataSets[$index]->track($matches[$index]);
        return true;
    }

    public function count()
    {
        return $this->count;
    }
}
