<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 07/02/2018
 * Time: 23:09
 */

namespace HttpReport\Core;


abstract class DataSet
{
    // Our list of data entries
    private $data = [];

    // a track of the total
    private $total = 0;

    // Return the user friendly name for this
    abstract function getName(): String;

    // Get the value before its sorted
    // This is to remove 'rogue' data like query strings etc
    abstract function getMutatedValue($value);

    public function track($dataToTrack)
    {
        // mutate our value
        $value = $this->getMutatedValue($dataToTrack);

        // if our mutaed value is null discard it
        // It means invalid data
        if ($value == null) {
            return;
        }

        // check if we already have a count for this key if not create it
        if ( ! isset($this->data[$value])) {
            $this->data[$value] = 0;
        }

        // increase our count by one
        $this->data[$value]++;
        $this->total++;
    }

    public function getData($order = 'asc', $limit = 10): array
    {
        if (strtolower($order) == 'desc') {  // Sort descending
            arsort($this->data, SORT_NUMERIC);
        } else { // Sort ascending
            asort($this->data, SORT_NUMERIC);
        }

        return array_slice($this->data, 0, $limit, true);
    }


    public function getExtendedData($order = 'desc', $limit = 10): array
    {
        $data  = $this->getData($order, $limit);
        $total = $this->total;

        // array_map doesn't preserve keys so we can't use functional programming
        $returnArray = [];

        foreach($data as $key => $count) {
            $returnArray[$key] =
                [
                    'count' => $count,
                    'percentage' => number_format(($count / $total) * 100, 2) . '%'
                ];
        }
        return $returnArray;
    }

    public function getTotal()
    {
        return $this->total;
    }
}
